package model

// Catalog contains a list of products
type Catalog struct {
	ID uint64 `gorm:"id,primaryKey" json:"id"`

	Name        string `gorm:"name" json:"name"`
	Description string `gorm:"description" json:"description"`
}

// CatalogOwner manages and updates a catalog
type CatalogOwner struct {
	Catalog   Catalog
	CatalogID uint64 `gorm:"catalog_id,primaryKey;autoIncrement:false"`
	User      User
	UserID    uint64 `gorm:"user_id,primaryKey;autoIncrement:false"`
}

// Product that can be ordered
type Product struct {
	ID uint64 `gorm:"id,primaryKey" json:"id"`

	ProductID   uint64 `gorm:"product_id" json:"product_id"`
	Name        string `gorm:"name" json:"name"`
	Description string `gorm:"description" json:"description"`

	Price   uint32 `gorm:"price" json:"price"`
	Deposit uint32 `gorm:"deposit" json:"deposit"`

	Catalog   Catalog `json:"catalog"`
	CatalogID uint64  `gorm:"catalog_id" json:"catalog_id"`
}
