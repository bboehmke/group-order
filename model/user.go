package model

// User account
type User struct {
	ID uint64 `gorm:"id,primaryKey" json:"id"`

	IsAdmin  bool   `gorm:"is_admin" json:"is_admin"`
	Name     string `gorm:"name" json:"name"`
	Email    string `gorm:"email" json:"email"`
	Password string `gorm:"password" json:"password"`
}
