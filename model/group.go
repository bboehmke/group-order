package model

// Group of users that collects products for an order of a catalog
type Group struct {
	ID uint64 `gorm:"id,primaryKey" json:"id"`

	Name        string  `gorm:"name" json:"name"`
	Description string  `gorm:"description" json:"description"`
	Catalog     Catalog `json:"catalog"`
	CatalogID   uint64  `gorm:"catalog_id"`
}

// GroupMember that can order or manage a group
type GroupMember struct {
	Group   Group
	GroupID uint64 `gorm:"group_id,primaryKey;autoIncrement:false"`
	User    User
	UserID  uint64 `gorm:"user_id,primaryKey;autoIncrement:false"`
	IsAdmin bool   `gorm:"is_admin" json:"is_admin"`
}
