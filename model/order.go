package model

import "time"

// GroupOrder scheduled by a group manager to collect products to order
type GroupOrder struct {
	ID uint64 `gorm:"id,primaryKey" json:"id"`

	Group   Group  `json:"group"`
	GroupID uint64 `gorm:"group_id" json:"group_id"`

	DueDate time.Time `gorm:"due_date" json:"due_date"`

	State interface{} // TBD
}

// OrderEntry that was added by an user to an order
type OrderEntry struct {
	ID uint64 `gorm:"id,primaryKey" json:"id"`

	GroupOrder   GroupOrder `json:"group_order"`
	GroupOrderID uint64     `gorm:"group_order_id" json:"group_order_id"`

	User   User   `json:"user"`
	UserID uint64 `gorm:"user_id" json:"user_id"`

	Product   Product `json:"product"`
	ProductID uint64  `gorm:"product_id" json:"product_id"`

	Amount uint `gorm:"amount" json:"amount"`

	// copied after order (to keep past values)
	Price   uint32 `gorm:"price" json:"price"`
	Deposit uint32 `gorm:"deposit" json:"deposit"`
}

// Bill a user has to pay for an previous order
type Bill struct {
	ID uint64 `gorm:"id,primaryKey" json:"id"`

	Date time.Time `gorm:"date" json:"date"`

	GroupOrder   GroupOrder `json:"group_order"`
	GroupOrderID uint64     `gorm:"group_order_id" json:"group_order_id"`

	User   User   `json:"user"`
	UserID uint64 `gorm:"user_id" json:"user_id"`

	Paid uint32 `gorm:"paid" json:"paid"`
}
