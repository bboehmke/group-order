# Data Model

```mermaid
graph BT
Group --> Catalog
Product --> Catalog

Order --> GroupOrder
Order --> Product
GroupOrder --> Group

Bill --> GroupOrder
Bill --> Order

Order --> User
Bill --> User

```
