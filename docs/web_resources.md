# Web paths

| Path                                       | Description                                                                                  |
| ------------------------------------------ | -------------------------------------------------------------------------------------------- |
| /catalogs                                  | List existing catalogs                                                                       |
| /catalogs/[CATALOG_ID]                     | Information of catalog                                                                       |
| /catalogs/[CATALOG_ID]/edit                | Modify catalogs (set description, add owner)                                                 |
| /catalogs/[CATALOG_ID]/products            | List of catalog products                                                                     |
| /groups                                    | List accessible groups                                                                       |
| /groups/[GROUP_ID]                         | Information of group                                                                         |
| /groups/[GROUP_ID]/edit                    | Modify group (select catalog, set description, add user)                                     |
| /groups/[GROUP_ID]/products                | List of group catalog products (option to add products to next order)                        |
| /groups/[GROUP_ID]/orders                  | List future and past scheduled orders                                                        |
| /groups/[GROUP_ID]/orders/[ORDER_ID]       | Information of order                                                                         |
| /groups/[GROUP_ID]/orders/[ORDER_ID]/bills | Bills of order (created after order complete)                                                |
| /user                                      | Information about current user (with option to change email & password, access to API token) |
| /user/bills                                | List of past bills of user                                                                   |
| /user/bills/[BILL_ID]                      | Bill information                                                                             |
| /admin/users                               | List of registered users                                                                     |
| /admin/users/[USER_ID]                     | User information                                                                             |

# Special paths

| Path        | Description                     |
| ----------- | ------------------------------- |
| /static/... | Static assets (CSS, JS, icons) |
